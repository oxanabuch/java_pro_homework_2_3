package com.company;

public class Main {

    public static void main(String[] args) {

        /**
         * Щомісячна капіталізація
         * Коли банк підсумовує прибуток за депозитом раз на місяць, розрахунок ведеться за формулою:
         * Дх = Р * (1 + (N/12)), зведене в ступінь Т)
         * де:
         * Дх — підсумковий дохід, тобто розмір вкладу на кінець терміну, включаючи початкову суму та нараховані проценти
         * P - початковий депозит
         * N - річна ставка, поділена на 100
         * T - термін договору в місяцях
         *
         * Процентний дохід визначається як різниця накопиченої суми вкладу та початкової
         */

        double initialAmount;
        double initialRate;
        double term;
        final int MONTHS_PER_YEAR = 12;
        double t;
        double amountFirstYear;
        double amountSecondYear;
        double amountThirdYear;
        double interestFirstYear;
        double interestSecondYear;
        double interestThirdYear;

        System.out.println("Java Deposit Calculator");
        System.out.println();
        System.out.print("Enter the deposit amount, UAH: " + args[0]);
        System.out.println();
        System.out.print("Enter the annual interest rate, %: " + args[1]);
        System.out.println();
        System.out.print("Enter the deposit term, years: " + args[2]);
        System.out.println();

        initialAmount = Double.parseDouble(args[0]);
        initialRate = Double.parseDouble(args[1]);
        term = Double.parseDouble(args[2]);
        t = term / term * MONTHS_PER_YEAR;
        System.out.println(initialAmount);
        System.out.println(initialRate);
        System.out.println(term);

        amountFirstYear = initialAmount * Math.pow((1 + ((initialRate / 100) / 12)), t);
        amountSecondYear = amountFirstYear * Math.pow((1 + ((initialRate / 100) / 12)), t);
        amountThirdYear = amountSecondYear * Math.pow((1 + ((initialRate / 100) / 12)), t);

        interestFirstYear = amountFirstYear - initialAmount;
        interestSecondYear = amountSecondYear - amountFirstYear;
        interestThirdYear = amountThirdYear - amountSecondYear;

        System.out.println("The accumulated amount for the first year: " + amountFirstYear);
        System.out.println("The accumulated amount for the second year: " + amountSecondYear);
        System.out.println("The accumulated amount for the third year: " + amountThirdYear);
        System.out.println();
        System.out.println("The income interest for the first year: " + interestFirstYear);
        System.out.println("The income interest for the second year: " + interestSecondYear);
        System.out.println("The income interest for the third year: " + interestThirdYear);
    }
}
